import { shallowMount } from "@vue/test-utils";
import FrameBody from "@/components/FrameBody.vue";

describe("FrameBody.vue", () => {
  it("renders props.msg when passed", () => {
    const msg = "new message";
    const wrapper = shallowMount(FrameBody, {
      props: { msg },
    });
    expect(wrapper.text()).toMatch(msg);
  });
});
